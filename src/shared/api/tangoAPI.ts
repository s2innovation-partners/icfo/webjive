import { request as graphqlRequest } from "graphql-request";
import MongoAPI from "./mongoAPI";
import {
  FETCH_ATTRIBUTES,
  FETCH_COMMANDS,
  FETCH_DEVICE_NAMES,
  FETCH_DEVICE_STATE,
  EXECUTE_COMMAND,
  SET_DEVICE_ATTRIBUTE,
  SET_DEVICE_PROPERTY,
  DELETE_DEVICE_PROPERTY,
  FETCH_ATTRIBUTE_METADATA,
  FETCH_ATTRIBUTES_VALUES,
  FETCH_DEVICE_METADATA,
  FETCH_DEVICE,
  FETCH_DATABASE_INFO,
  ATTRIBUTES_SUB_WITH_VALUES,
  ATTRIBUTES_SUB_WITHOUT_VALUES
} from "./graphqlQuery";

import {
  FetchAttributes,
  FetchCommands,
  FetchDeviceNames
} from "./graphqlQuery";

function request<T = any>(
  tangoDB: string,
  query: string,
  args?: object
): Promise<T> {
  const url = `/${tangoDB}/db`;
  return graphqlRequest(url, query, args || {});
}

function socketUrl(tangoDB) {
  const loc = window.location;
  const protocol = loc.protocol.replace("http", "ws");
  return protocol + "//" + loc.host + "/" + tangoDB + "/socket";
}

function createSocket(tangoDB) {
  return new WebSocket(socketUrl(tangoDB), "graphql-ws");
}

export default {
  async fetchDeviceAttributes(tangoDB: string, device: string) {

    try {
      const data = await request<FetchAttributes>(tangoDB, FETCH_ATTRIBUTES, {
        device
      });
      const { attributes } = data.device;
      if (attributes != null) {
        return attributes;
      } else {
        // Some kind of error reporting could go here. This should only happen when the attributes resolver in the backend fails for some unexpected reason. For now, just return an empty list.
        return [];
      }
    } catch (err) {
      return [];
    }
  },

  async fetchDatabaseInfo(tangoDB: string) {
    const data = await request(tangoDB, FETCH_DATABASE_INFO);
    return data.info;
  },

  async fetchCommands(tangoDB: string, device: string) {
    try {
      const data = await request<FetchCommands>(tangoDB, FETCH_COMMANDS, {
        device
      });
      return data.device.commands;
    } catch (err) {
      return [];
    }
  },

  async fetchDeviceNames(tangoDB: string) {
    try {
      const data = await request<FetchDeviceNames>(tangoDB, FETCH_DEVICE_NAMES);
      return data.devices.map(({ name }) => name.toLowerCase());
    } catch (err) {
      return [];
    }
  },

  async executeCommand(
    tangoDB: string,
    device: string,
    command: string,
    argin?: any
  ) {
    try {
      const args = argin? { device, command, argin } : {device, command};
      const data = await request(tangoDB, EXECUTE_COMMAND, args);
      const { ok, output } = data.executeCommand;
      if (ok) {
        const timestamp = new Date();
        MongoAPI.saveUserAction({
          actionType: "ExcuteCommandUserAction",
          timestamp,
          tangoDB,
          device,
          name: command,
          argin
        });
      }
      return { ok, output };
    } catch (err) {
      return null;
    }
  },

  async setDeviceAttribute(
    tangoDB: string,
    device: string,
    name: string,
    value: any
  ) {
    try {
      const args = { device, attribute: name, value };
      const data = await request(tangoDB, SET_DEVICE_ATTRIBUTE, args);
      const { ok, valueBefore, attribute } = data.setAttributeValue;
      if (ok) {
        const timestamp = new Date();
        MongoAPI.saveUserAction({
          actionType: "SetAttributeValueUserAction",
          timestamp,
          tangoDB,
          device,
          name: attribute.name,
          valueBefore,
          valueAfter: attribute.value,
          value,
        });
      }
      return { ok, attribute };
    } catch (err) {
      return { ok: false, attribute: null };
    }
  },

  async setDeviceProperty(
    tangoDB: string,
    device: string,
    name: string,
    value: any
  ) {
    const args = { device, name, value };
    const data = await request(tangoDB, SET_DEVICE_PROPERTY, args);
    const { ok } = data.putDeviceProperty;
    if (ok) {
      const timestamp = new Date();
      MongoAPI.saveUserAction({
        actionType: "PutDevicePropertyUserAction",
        timestamp,
        tangoDB,
        device,
        name,
        value,
      });
    }
    return data.putDeviceProperty.ok;
  },

  async deleteDeviceProperty(
    tangoDB: string,
    device: string,
    name: string) {
    const args = { device, name };
    const data = await request(tangoDB, DELETE_DEVICE_PROPERTY, args);
    const { ok } = data.deleteDeviceProperty;
    if (ok) {
      const timestamp = new Date();
      MongoAPI.saveUserAction({
        actionType: "DeleteDevicePropertyUserAction",
        timestamp,
        tangoDB,
        device,
        name
      });
    }
    return ok;
  },
  async fetchDevice(
    tangoDB: string,
    name: string) {
    const args = { name };

    let device:any = null;
    let errors = [];

    try {
      const data = await request(tangoDB, FETCH_DEVICE, args);
      device = data.device;
      if (device === null) {
        return null;
      }
      return { ...device, name: device.name.toLowerCase(), errors };
    } catch (err) {
      // The structure of errors is currently not ideal and will probably undergo change. Update this logic accordingly.
      errors = err.response.errors[0];
      device = err.response.data.device;
      return null;
    }
    
  },

  async fetchAttributeMetadata(
    tangoDB: string,
    fullNames: string[]
  ) {
    for(var i=0;i<fullNames.length;i++) fullNames[i] = fullNames[i].toLowerCase();
    const data = await request(tangoDB, FETCH_ATTRIBUTE_METADATA, { fullNames });
    const result = {};

    for (const attribute of data.attributes) {
      const { device, name, dataformat, datatype, label, enumLabels, unit } = attribute;

      const fullName = device + "/" + name;
      const dataFormat = dataformat.toLowerCase();
      const dataType = datatype;
      const enumlabels = enumLabels;

      result[fullName] = { dataFormat, dataType, unit, enumlabels, label};
    }
    return result;
  },

  async fetchDeviceState(
    tangoDB: string,
    name: string
  ) {
    try {
      const args = { name };
      const data = await request(tangoDB, FETCH_DEVICE_STATE, args);
      return data.device.state;
    } catch (err) {
      return null;
    }
  },

  async fetchAttributesValues(
    tangoDB: string,
    fullNames: string[]
  ): Promise<
    Array<{
      name: string;
      device: string;
      value: any;
      writevalue: any;
      timestamp: number;
    }>
  > {
    try {
      const data = await request(tangoDB, FETCH_ATTRIBUTES_VALUES, { fullNames });
      return data.attributes;
    } catch (err) {
      return [];
    }
  },

  async fetchDeviceMetadata(
    tangoDB: string,
    deviceNames: string[]
  ) {
    const result = {};

    for (const deviceName of deviceNames) {
      let data: any;
      try {
        data = await request(tangoDB, FETCH_DEVICE_METADATA, { deviceName });
      } catch (err) {
        return null;
      }
      if (data.device) {
        const { alias } = data.device;
        result[deviceName] = { alias };
      }
    }

    return result;
  },
  changeEventEmitter(
    tangoDB: string, 
    fullNames: string[], 
    includeValues?: boolean) {
    includeValues = includeValues === undefined ? true : includeValues;

    const socket = createSocket(tangoDB);
    const query = includeValues
      ? ATTRIBUTES_SUB_WITH_VALUES
      : ATTRIBUTES_SUB_WITHOUT_VALUES;

    return emit => {
      socket.addEventListener("open", () => {
        const variables = { fullNames };
        const startMessage = JSON.stringify({
          type: "start",
          payload: {
            query,
            variables
          }
        });
        socket.send(startMessage);
      });

      socket.addEventListener("error", err => {
        emit(err);
      });

      socket.addEventListener("message", msg => {
        const frame = JSON.parse(msg.data);
        if (frame.type === "data" && frame.payload.errors == null) {
          const attribute = frame.payload.data.attributes;
          const device = attribute.device.toLowerCase();
          emit({ ...attribute, device });
        }
      });

      return () => {
        socket.close();
      };
    };
  }
}
