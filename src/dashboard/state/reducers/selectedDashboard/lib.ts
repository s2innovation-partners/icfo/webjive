import {
  Widget,
  IndexPath,
  WidgetDefinition,
  InputDefinitionMapping,
  InputMapping,
  InputDefinition,
  DashboardEditHistory
} from "../../../types";
import { defaultInputs } from "../../../utils";
import { definitionForWidget } from "../../../widgets";

const HISTORY_SIZE = 100;

export function move(widget: Widget, dx: number, dy: number) {
  const { x, y } = widget;
  const targetX = Math.max(0, x + dx);
  const targetY = Math.max(0, y + dy);
  return { ...widget, x: targetX, y: targetY };
}
export function undo(
  history: DashboardEditHistory,
  widgets: Record<string, Widget>
) {
  if (history.undoLength === 0) {
    return { history, widgets };
  }
  //pull latest value from UNDO
  history.undoIndex =
    history.undoIndex === 0 ? HISTORY_SIZE - 1 : history.undoIndex - 1;
  const prevWidgets = history.undoActions[history.undoIndex];
  history.undoLength = history.undoLength === 0 ? 0 : history.undoLength - 1;

  //push latest value from UNDO onto REDO
  history.redoLength =
    history.redoLength === HISTORY_SIZE ? HISTORY_SIZE : history.redoLength + 1;
  history.redoIndex = (history.redoIndex + 1) % HISTORY_SIZE;
  history.redoActions[history.redoIndex] = widgets;
  return { history, widgets: prevWidgets };
}

export function redo(
  history: DashboardEditHistory,
  widgets: Record<string, Widget>
) {
  if (history.redoLength === 0) {
    return { history, widgets };
  }
  //push old widget as new action to UNDO
  history.undoLength =
    history.undoLength === HISTORY_SIZE ? HISTORY_SIZE : history.undoLength + 1;
  history.undoActions[history.undoIndex] = widgets;
  history.undoIndex = (history.undoIndex + 1) % HISTORY_SIZE;

  //pull, update and return from REDO
  const prevWidgets = history.redoActions[history.redoIndex];
  history.redoLength = history.redoLength === 0 ? 0 : history.redoLength - 1;
  history.redoIndex =
    history.redoIndex === 0 ? HISTORY_SIZE - 1 : history.redoIndex - 1;

  return { history, widgets: prevWidgets };
}

export function pushToHistory(
  history: DashboardEditHistory,
  widgets: Record<string, Widget>
) {
  history.undoLength =
    history.undoLength === HISTORY_SIZE ? HISTORY_SIZE : history.undoLength + 1;
  history.undoActions[history.undoIndex] = widgets;
  history.undoIndex = (history.undoIndex + 1) % HISTORY_SIZE;
  //invalidate REDO stack at a regular action
  history.redoLength = 0;
  return history;
}

export function resize(
  widget: Widget,
  mx: number,
  my: number,
  dx: number,
  dy: number
) {
  const moved = move(widget, mx, my);
  const { width, height } = moved;
  return {
    ...moved,
    width: Math.max(2, width + dx),
    height: Math.max(2, height + dy)
  };
}

/**
 * Compares the inputs found in the widget from the database with those found in the widget definition in webjive
 * If an input is found in the database that doesn't exist in the widget definition, this input is removed and a warning is returned
 * If an input is found in the widget definition that doesn't exist in the database widget, webjive creates it and assigns it
 * its default value.
 * @param widgets The widgets of the selected dashboard, exactly as retrieved from the database
 * @returns An object containing
 *  1) the the same widgets with inputs added or deleted to match the input Definition
 *  2) a boolean warning, indicating if the widget inputs had to be altered to comply with the lastest webjive input definitions
 */

export function resolveWidgetCompatibility(widgets: Widget[]) {
  let warning: boolean = false;
  widgets.forEach(widget => {
    const definition = definitionForWidget(widget);
    for (const key of Object.keys(definition.inputs)) {
      if (!(key in widget.inputs) && "default" in definition.inputs[key]) {
        widget.inputs[key] = definition.inputs[key].default;
        warning = true;
      }
      const newInputs: InputMapping = {};

      // eslint-disable-next-line no-loop-func
      Object.keys(widget.inputs).forEach(input => {
        if (definition.inputs[input]) {
          newInputs[input] = widget.inputs[input];
        } else {
          warning = true;
        }
      });
      widget.inputs = newInputs;
    }
  });
  return { widgets, warning };
}
export function validate(widget: Widget) {
  const definition = definitionForWidget(widget);
  const valid = inputsAreValid(definition!.inputs, widget.inputs);
  return { ...widget, valid };
}

export function setInput(widget: Widget, path: IndexPath, value: any, widgetType: string) {
  const oldInputs = widget.inputs;

  const widgetsWithMultipleCommands = ['CONTAINER_FOR_DEVICE'];

  let newInputs;
  if (widgetsWithMultipleCommands.indexOf(widgetType) > -1) {
    newInputs = setWithIndexPath(oldInputs, path, value, MULTIPLE_COMMANDS);
  } else {
    newInputs = setWithIndexPath(oldInputs, path, value, REPLACE);
  }

  return { ...widget, inputs: newInputs };
}

export function deleteInput(widget: Widget, path: IndexPath) {
  const oldInputs = widget.inputs;
  const newInputs = setWithIndexPath(oldInputs, path, null, DELETE);
  return { ...widget, inputs: newInputs };
}

export function addInput(widget: Widget, path: IndexPath, value: any) {
  const oldInputs = widget.inputs;
  const newInputs = setWithIndexPath(oldInputs, path, value, ADD);
  return { ...widget, inputs: newInputs };
}

const DELETE = Symbol("DELETE");
const ADD = Symbol("ADD");
const REPLACE = Symbol("REPLACE");
const MULTIPLE_COMMANDS = Symbol("MULTIPLE_COMMANDS");

type Mode = typeof DELETE | typeof ADD | typeof REPLACE | typeof MULTIPLE_COMMANDS;

function handleMultipleCommands(
  obj : object,
  head: string | number,
  value: any,
  replacement: any,
) {
  let values : string[] = [];

  if (typeof obj[head].command === "string") {
    values.push(obj[head].command);
  } else if (obj[head].command && obj[head].command.length > 0) {
    values = [...obj[head].command];
  }

  if (value.command) {
    if (values.indexOf(value.command) === -1) {
      values.push(value.command);
    } else {
      values = values.filter((val) => val !== value.command);
    }
  }

  replacement.command = values;

  return replacement;
}
export function setWithIndexPath(
  obj: object,
  path: IndexPath,
  value: any,
  mode: Mode
) {
  const [head, ...tail] = path;
  let replacement =
    tail.length > 0 ? setWithIndexPath(obj[head], tail, value, mode) : value;

  if (Array.isArray(obj)) {
    const copy = obj.concat();
    if (typeof head !== "number") {
      throw new Error("head must be an integer when obj is an array");
    } else {
      if (mode === DELETE) {
        copy.splice(head, 1);
      } else if (mode === REPLACE) {
        copy.splice(head, 1, replacement);
      } else if (mode === ADD) {
        // Some logic to support both negative and positive indices. Needs to be tested more thorougly
        const length = copy.length;
        const norm = length > 0 ? (length + head) % length : 0;
        const index = norm + (head < 0 ? 1 : 0);
        copy.splice(index, 0, replacement);
      }
    }
    return copy;
  } else {
    if (mode === MULTIPLE_COMMANDS && head === "command") {
      replacement = handleMultipleCommands(obj, head, value, replacement);
    }

    return { ...obj, [head]: replacement };
  }
}

export function defaultDimensions(
  definition: WidgetDefinition<{}>
): { width: number; height: number } {
  const { defaultWidth: width, defaultHeight: height } = definition;
  return { width, height };
}

export function nestedDefault(
  definition: WidgetDefinition<{}>,
  path: IndexPath
) {
  const initial = { inputs: definition.inputs };
  const leaf = path.reduce((accum, segment): {
    inputs: InputDefinitionMapping;
  } => {
    const input = accum.inputs[segment];
    if (typeof segment === "number") {
      return accum;
    } else if (input.type === "complex") {
      return input;
    } else {
      throw new Error("only complex inputs can be traversed");
    }
  }, initial);
  return defaultInputs(leaf.inputs);
}

// TODO: cover more validation cases
function inputIsValid(definition: InputDefinition, value: any): boolean {
  if (definition.type === "complex") {
    if (definition.repeat) {
      return value
        .map(input => inputIsValid({ ...definition, repeat: false }, input))
        .reduce((prev, curr) => prev && curr, true);
    } else {
      const inputNames = Object.keys(definition.inputs);
      return inputNames
        .map(name => inputIsValid(definition.inputs[name], value[name]))
        .reduce((prev, curr) => prev && curr, true);
    }
  }

  if (definition.type === "attribute") {
    const resolvedDevice = value.device || definition.device;
    const resolvedAttribute = value.attribute || definition.attribute;
    return resolvedDevice != null && resolvedAttribute != null;
  }

  if (definition.type === "command") {
    const resolvedDevice = value.device || definition.device;
    const resolvedCommand = value.command || definition.command;
    return resolvedDevice != null && resolvedCommand != null;
  }

  if (definition.type === "number") {
    if (isNaN(value)) {
      return false;
    }

    if (definition.nonNegative) {
      return value >= 0;
    }
  }

  if (definition.type === "device") {
    return value != null;
  }

  return true;
}

export function inputsAreValid(
  definition: InputDefinitionMapping,
  inputs: InputMapping
): boolean {
  const inputNames = Object.keys(definition);
  const results = inputNames.map(name => {
    const inputDefinition = definition[name];
    const input = inputs[name];
    return inputIsValid(inputDefinition, input);
  });

  return results.reduce((prev, curr) => prev && curr, true);
}

export function nextId(widgets: Record<string, Widget>): string {
  const ids = Object.keys(widgets).map(key => parseInt(key, 10));
  const highest = ids.reduce((max, id) => Math.max(max, id), 0);
  return String(1 + highest);
}

export function nextOrderIndex(widgets: Record<string, Widget>) {
  const orders = Object.values(widgets).map(value => {
    if (!value.hasOwnProperty("order")) {
      return -1;
    }
    return value.order;
  });
  return orders.reduce((max, current) => Math.max(max, current), -1) + 1;
}

/**
 * Reassigns all order indexes after the removal of 1 or more layers
 * @param widgets
 */
export function reorderIndex(widgets: Record<string, Widget>) {
  let currentIndex = 0;
  Object.values(widgets)
    .sort((a, b) => a.order - b.order)
    .forEach(widget => {
      widget.order = currentIndex;
      currentIndex++;
    });
  return widgets;
}
/**
 * Given allWidgets, moves the widgets in widgetsToMove 1 step up or down. The location of a widget is determined by widget.order.
 * Returns a copy of allWidgets, with updated order of the widgets. If widgets cannot be moved in the specified direction, returns null
 * @param widgets
 * @param sourceIndex
 * @param up
 */
export function moveOrderIndices(
  allWidgets: Widget[],
  widgetsToMove: Widget[],
  up: boolean
): Widget[] | null {
  //If up, take no action if one of the widgets already is at the top of the list
  if (up && widgetsToMove.find(widget => widget.order === 0)) {
    return null;
  }
  if (!up) {
    //If down, take no action if one of the widgets already is at the bottom of the list
    const maxOrder = allWidgets.reduce(
      (prev, curr) => Math.max(prev, curr.order),
      0
    );
    if (widgetsToMove.find(widget => widget.order === maxOrder)) {
      return null;
    }
  }
  const newWidgets: Widget[] = [];
  const widgetsToMoveIds = widgetsToMove.map(widget => widget.id);
  const assignedOrders: number[] = [];
  widgetsToMove.forEach(widget => {
    const copy = { ...widget };
    copy.order = widget.order + (up ? -1 : 1);
    assignedOrders.push(copy.order);
    newWidgets.push(copy);
  });
  let currNewOrder = 0;
  allWidgets
    .sort((a, b) => a.order - b.order)
    .forEach(widget => {
      if (!widgetsToMoveIds.includes(widget.id)) {
        while (assignedOrders.includes(currNewOrder)) {
          currNewOrder = currNewOrder + 1;
        }
        const copy = { ...widget };
        copy.order = currNewOrder;
        newWidgets.push(copy);
        assignedOrders.push(currNewOrder);
      }
    });
  return newWidgets;
}
