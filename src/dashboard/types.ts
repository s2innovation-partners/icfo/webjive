import React from "react";

export interface BaseInputDefinition<T> {
  label?: string;
  repeat?: boolean;
  default?: T;
  required?: boolean;
  title?: string;
}

export interface Notification {
  level: string;
  sourceAction: string;
  msg: string;
}

export interface BooleanInputDefinition extends BaseInputDefinition<boolean> {
  type: "boolean";
}

export interface NumberInputDefinition extends BaseInputDefinition<number> {
  type: "number";
  nonNegative?: boolean;
}

export interface StringInputDefinition extends BaseInputDefinition<string> {
  type: "string";
  renderAs?: "text" | "textarea";
  placeholder?: string;

}
export interface StyleInputDefinition extends BaseInputDefinition<string>{
  type: "style";
}

export interface ComplexInputDefinition<T = any>
  extends BaseInputDefinition<null> {
  type: "complex";
  inputs: T;
}

interface SelectInputDefinitionOption<T> {
  name: string;
  value: T; // ?
}

export interface SelectInputDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "select";
  options: SelectInputDefinitionOption<T>[];
}

export interface AttributeInputDefinition
  extends BaseInputDefinition<{
    device: null;
    attribute: null;
    label: null;
  }> {
  type: "attribute";
  dataFormat?: "scalar" | "spectrum" | "image";
  dataType?: "numeric";
  device?: string;
  attribute?: string;
  label?: string;
  invalidates?: string[];
}

export interface ColorInputDefinition extends BaseInputDefinition<string> {
  type: "color";
}

export interface DeviceInputDefinition extends BaseInputDefinition<null> {
  type: "device";
  publish?: string;
}

export interface CommandInputDefinition extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: string;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
}

export interface CommandInputDefinitionMultiple extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: Array<string>;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
}

export interface CommandInputWithParameterDefinition extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: string;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
}

export type InputDefinition =
  | BooleanInputDefinition
  | NumberInputDefinition
  | StringInputDefinition
  | ComplexInputDefinition
  | AttributeInputDefinition
  | SelectInputDefinition
  | ColorInputDefinition
  | DeviceInputDefinition
  | CommandInputDefinition
  | CommandInputDefinitionMultiple
  | StyleInputDefinition;

export interface InputDefinitionMapping {
  [name: string]: InputDefinition;
}

export interface Widget {
  _id: string; //not really used, but automatically created by mongodb
  type: string;
  id: string;
  valid: boolean;
  canvas: string;
  x: number;
  y: number;
  width: number;
  height: number;
  inputs: InputMapping;
  order: number;
}

//meta information about available group dashboards, with count and whether it has been loade for each group
export interface AvailableGroupDashboards{
  [group: string]: {
    count: number,
    loaded: boolean,
  }
}
export interface SharedDashboards{
  dashboards: Dashboard[];
  availableGroupDashboards: AvailableGroupDashboards;
}
export interface DashboardEditHistory{
  undoActions: Record<string, Widget>[],
  redoActions: Record<string, Widget>[],
  undoIndex: number,
  redoIndex: number,
  undoLength: number,
  redoLength: number,
}

export interface Dashboard {
  id: string;
  name: string;
  user: string;
  insertTime: Date | null;
  updateTime: Date | null;
  group: string | null;
  groupWriteAccess:boolean;
  lastUpdatedBy: string | null;
  selectedIds: string[];
  history: DashboardEditHistory;
}

export interface InputMapping {
  [name: string]: any;
}

export interface WidgetDefinition<T extends InputDefinitionMapping> {
  type: string;
  name: string;
  defaultWidth: number;
  defaultHeight: number;
  inputs: T;
}

export interface WidgetBundle<T extends InputDefinitionMapping> {
  definition: WidgetDefinition<T>;
  component: React.ElementType;
}

export type IndexPath = Array<string | number>;

interface AttributeValue<ValueT> {
  value: ValueT;
  writeValue: ValueT;
  timestamp: number;
}

export interface AttributeInput<ValueT = any> extends AttributeValue<ValueT> {
  device: string;
  attribute: string;
  label: string;
  history: Array<AttributeValue<ValueT>>;
  dataType: string;
  dataFormat: string;
  enumlabels: Array<string>;
  isNumeric: boolean;
  unit: string;
  write: (value: ValueT) => void;
}

export interface CommandInput<OutputT = any> {
  device: string;
  command: string;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (argin?: any) => void;
}

export interface CommandInputMultiple<OutputT = any> {
  device: string;
  command: Array<string>;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (argin?: any, command?: string) => void;
}

export interface DeviceInput {
  name: string;
  alias: string;
}
export interface StyleInput{
  [key:string]: any
}


export interface CommandInputWithParameter<OutputT = any> {
  device: string;
  command: string;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (value: OutputT) => void;
}

export type ComplexInput<T> = any;

export interface Canvas {
  id: string;
  name: string;
}
