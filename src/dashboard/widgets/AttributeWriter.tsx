import React, { Component, FormEvent, CSSProperties } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  AttributeInputDefinition,
  BooleanInputDefinition,
  ColorInputDefinition,
  NumberInputDefinition,
  SelectInputDefinition,
  AttributeInput
} from "../types";

type Inputs = {
  attribute: AttributeInputDefinition;
  showDevice: BooleanInputDefinition;
  showAttribute: SelectInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  input: string;
  pending: boolean;
}

class AttributeWriter extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      input: "",
      pending: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    const { mode, inputs } = this.props;
    const {
      attribute,
      showDevice,
      showAttribute,
      backgroundColor,
      textColor,
      size,
      font
    } = inputs;
    const { device, writeValue, attribute: attributeName, label } = attribute;

    const unit = mode === "run" ? attribute.unit : "unit";
    const deviceLabel = device || "device";

    let display = this.getDisplay(attribute,showAttribute);

    const displayWidget = [
      ...(showDevice ? [deviceLabel] : []),
      display
    ].join("/");

    const dataType = this.dataType();
    if (
      mode === "run" &&
      dataType !== "numeric" &&
      dataType !== "string" &&
      dataType !== "boolean"
    ) {
      return (
        <div style={{ backgroundColor: "red", padding: "0.5em" }}>
          {attribute.dataType} not implemented
        </div>
      );
    }

    const isInvalid =
      (dataType === "numeric" && isNaN(Number(this.state.input))) ||
      (dataType === "boolean" &&
        ["true", "false", ""].indexOf(this.state.input.toLowerCase()) === -1);
    const invalidStyle = isInvalid ? { outline: "1px solid red" } : {};
    const placeholder =
      dataType === "boolean"
        ? writeValue === true
          ? "true"
          : "false"
        : writeValue || "";
    const style: CSSProperties = {
      display: "flex",
      alignItems: "center",
      padding: "0.25em 0.5em",
      backgroundColor,
      color: textColor,
      fontSize: size + "em"
    };
    if (font) {
      style["fontFamily"] = font;
    }
    return (
      <form style={style} onSubmit={this.handleSubmit}>
        {displayWidget && (
          <span style={{ flexGrow: 0, marginRight: "0.5em" }}>{displayWidget}:</span>
        )}
        <input
          type="text"
          style={{
            flexGrow: 1,
            minWidth: "3em",
            ...invalidStyle
          }}
          placeholder={placeholder}
          value={this.state.input}
          onChange={e => this.setState({ input: e.target.value })}
        />
        {unit && <span style={{ marginLeft: "0.5em" }}>{unit}</span>}
      </form>
    );
  }

  private dataType(): "numeric" | "boolean" | "string" | "other" {
    const { attribute } = this.props.inputs;
    const { dataType, isNumeric } = attribute;
    const isBoolean = dataType === "DevBoolean";
    const isString = dataType === "DevString";
    return isNumeric
      ? "numeric"
      : isBoolean
      ? "boolean"
      : isString
      ? "string"
      : "other";
  }

  private getDisplay(attribute: AttributeInput,showAttribute: string): string {
    let display = "";
    if(showAttribute === "Label") {
      if(attribute.label!=="") display = attribute.label;
      else display = "attributeLabel";
    } 
    else if(showAttribute === "Name"){
      if(attribute.attribute!==null) display = attribute.attribute;
      else display = "attributeName";
    } 
    return display;
  }

  private async handleSubmit(event: FormEvent<HTMLFormElement>) {
    if (this.state.pending) {
      return;
    }

    event.preventDefault();
    const { attribute } = this.props.inputs;
    const { isNumeric } = attribute;
    const isBoolean = attribute.dataType === "DevBoolean";
    const { input } = this.state;
    let value: any = input;
    if ((isNumeric || isBoolean) && input === "") {
      return; //we don't want to interpret an emtpy string as a zero or false
    }
    if (isNumeric) {
      value = Number(input);
    }
    if (isBoolean) {
      value = input.toLowerCase() === "false" ? false : true;
    }
    if (typeof value === "number" && isNaN(value)) {
      return;
    }

    this.setState({ input: "", pending: true });
    await this.props.inputs.attribute.write(value);
    this.setState({ pending: false });
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "ATTRIBUTE_WRITER",
  name: " Attribute Writer",
  defaultHeight: 2,
  defaultWidth: 15,
  inputs: {
    attribute: {
      type: "attribute",
      label: "",
      dataFormat: "scalar"
    },
    showDevice: {
      type: "boolean",
      label: "Show Device Name",
      default: true
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label"
        },
        {
          name: "Name",
          value: "Name"
        },
        {
          name: "None",
          value: "None"
        }
      ]
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000"
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff"
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica"
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new"
        }
      ]
    }
  }
};

export default {
  definition,
  component: AttributeWriter
};
