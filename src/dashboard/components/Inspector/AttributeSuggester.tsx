import React, { Component } from "react";
import Autosuggest from "react-autosuggest";
import alphanumSort from "alphanum-sort";

import "./DeviceSuggester.css";

interface State {
  value: string;
  attribute: string;
  suggestions: string[];
  labels: string[];
}

interface Props {
  attributes: string[];
  labels: string[];
  attributeLabel: string | undefined;
  attributeName: string | undefined;
  hasDevice: boolean;
  onSelection: (newValue: string) => void;
}

export default class AttributeSuggester extends Component<Props, State> {
  constructor(props) {
    super(props);
    const { attributes, attributeName, attributeLabel, labels } = this.props;
    this.state = {
      value: attributeLabel || "",
      attribute: attributeName || "",
      labels: labels || [],
      suggestions: attributes || []
    };

    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
    this.onChange = this.onChange.bind(this);
    this.storeInputReference = this.storeInputReference.bind(this);
  }

  public componentDidUpdate(prevProps: Props) {
    const { attributeLabel } = this.props;
    if (attributeLabel !== prevProps.attributeLabel) {
      this.setState({ value: attributeLabel || "" });
    }
  }

  public renderSuggestion = (suggestion: string) => {
    // deal with highlighting of matching texts

    const { value } = this.state;
    const index = suggestion.toLowerCase().indexOf(value.toLowerCase());
    if (index === -1) {
      // no highlight, e.g. when value === "*"
      return <div>{suggestion}</div>;
    }
    // highlight everything that matches
    return (
      <div>
        {suggestion.substring(0, index)}
        <b>{suggestion.substring(index, index + value.length)}</b>
        {suggestion.substring(index + value.length)}
      </div>
    );
  };

  public storeInputReference(autosuggest: Autosuggest): void {
    if (autosuggest !== null) {
      autosuggest.input.spellcheck = false;
      autosuggest.input.onfocus = () => {
        autosuggest.input.select();
      };
    }
  }

  public render(): Autosuggest {
    let { value, suggestions } = this.state;
    const placeHolder = this.props.hasDevice ? "Type in an attribute (or *)" : "Pick a device first";
    const inputProps = {
      placeholder: placeHolder,
      value,
      onChange: this.onChange
    };

    const theme = {
      ...Autosuggest.defaultProps.theme,
      input: "form-control react-autosuggest__input"
    };
    if(!this.props.hasDevice){
      return null;
    }
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        onSuggestionSelected={this.onSuggestionSelected}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        highlightFirstSuggestion={true}
        ref={this.storeInputReference}
        inputProps={inputProps}
        theme={theme}
      /> 
    )
  }

  public getSuggestions(value: string): string[] {
    if (value.trim() === "") {
      return [];
    }
    if (value.trim() === "*") {
      return this.props.labels.slice();
    }

    return this.props.labels.filter(label => label.toLowerCase().startsWith(value.trim().toLowerCase()));
  }

  // The suggester is unusably slow and resource-demanding if the list is not truncated. This is just a quickfix; there's probably a more sophisticated way such as using react-window
  public getTruncatedSuggestions(value: string): string[] {
    return this.getSuggestions(value).slice(0, 100);
  }

  public onSuggestionSelected(event, { suggestion, suggestionValue }): void {
    this.props.onSelection(suggestionValue);
  }

  public onChange = (event, { newValue, method }): void => {
    this.setState({
      value: newValue
    });
  };

  public onSuggestionsFetchRequested = ({ value }): void => {
    this.setState({
      suggestions: alphanumSort(this.getTruncatedSuggestions(value))
    });
  };

  public onSuggestionsClearRequested = (): void => {
    this.setState({
      suggestions: []
    });
  };
}

const getSuggestionValue = (suggestion: string) => suggestion;
