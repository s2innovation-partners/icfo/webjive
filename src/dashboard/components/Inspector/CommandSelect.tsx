import React, { Component, FormEvent, ChangeEvent } from "react";
import { DeviceConsumer } from "../DevicesProvider";
import DeviceSuggester from "./DeviceSuggester";
import TangoAPI from "../../../shared/api/tangoAPI";

interface Command {
  name: string;
  intype: string;
}

interface Props {
  tangoDB: string;
  device: string;
  command: string | Array<string>;
  inputType?: string;
  onSelect: (device: string, command: string | null) => void;
  selectMultipleCommands: boolean;
}

interface State {
  fetchingCommands: boolean;
  commands: Command[];
}

export default class CommandSelect extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = { fetchingCommands: false, commands: [] };
    this.handleSelectDevice = this.handleSelectDevice.bind(this);
    this.handleSelectCommand = this.handleSelectCommand.bind(this);
    this.handleSelectMultipleCommands = this.handleSelectMultipleCommands.bind(this);
  }

  public componentDidMount() {
    this.fetchCommands();
  }

  public componentDidUpdate(prevProps) {
    if (this.props.device !== prevProps.device) {
      this.setState({ commands: [] });
      this.fetchCommands();
    }
  }

  public handleSelectDevice(newDevice: string) {
    this.fetchCommands();
    const { onSelect } = this.props;
    if (onSelect && newDevice) {
      onSelect(newDevice, null);
    }
  }

  public handleSelectMultipleCommands(event: ChangeEvent<HTMLInputElement>) {
    const { onSelect, device } = this.props;
    const newCommand = event.currentTarget.value;
    if (onSelect && device && newCommand) {
      onSelect(device, newCommand);
    }
  }

  public handleSelectCommand(event: FormEvent<HTMLSelectElement>) {
    const { onSelect, device } = this.props;
    const newCommand = event.currentTarget.value;
    if (onSelect && device && newCommand) {
      onSelect(device, newCommand);
    }
  }

  public render() {
    const { device, command, selectMultipleCommands } = this.props;
    const commands = this.filteredCommands();
    
    return (
      <DeviceConsumer>
        {({ devices }) => {
          const hasDevice = device != null && device !== "";
          const hasCommands = commands.length > 0;

          return (
            <div className="CommandSelect">
              <DeviceSuggester
                deviceName={device}
                devices={devices}
                onSelection={newValue => this.handleSelectDevice(newValue)}
              />
              {
                selectMultipleCommands && hasDevice && hasCommands ? (
                  commands.map(({name}) => (
                    <div key={name} style={{ padding: "2px 5px", display: "flex", alignItems: "center" }}>
                      <input
                        type="checkbox"
                        value={name}
                        onChange={this.handleSelectMultipleCommands}
                        checked={command.indexOf(name) !== -1}
                      />
                      <span style={{ margin: "0 0 0 2px" }}>{name}</span>
                    </div>
                  ))
                ) : (
                  <select
                    className="form-control"
                    value={command || ""}
                    disabled={hasCommands === false}
                    onChange={this.handleSelectCommand}
                  >
                    {hasDevice === false && (
                      <option value="">
                        Pick a device first
                      </option>
                    )}
                    {hasDevice && hasCommands === false && (
                      <option value="" >
                        No commands
                      </option>
                    )}
                    {hasDevice && hasCommands && (
                      <option value="" disabled={true} >
                        Select command...
                      </option>
                    )}
                    {commands.map(({ name }, i) => (
                      <option key={i} value={name}>
                        {name}
                      </option>
                    ))}
                  </select>
                )
              }
            </div>
          );
        }}
      </DeviceConsumer>
    );
  }

  private filteredCommands() {
    const { commands } = this.state;
    const { inputType } = this.props;

    if (inputType != null) {
      return commands.filter(({ intype }) => intype === inputType);
    }

    return commands;
  }

  private async fetchCommands() {
    const { device, tangoDB } = this.props;
    if (device) {
      this.setState({ commands: [], fetchingCommands: true });
      const commands = await TangoAPI.fetchCommands(tangoDB, device);
      this.setState({ commands, fetchingCommands: false });
    }
  }
}
