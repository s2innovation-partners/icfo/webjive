import React, { Component } from "react";

import TangoAPI from "../../../shared/api/tangoAPI";
import { DeviceConsumer } from "../DevicesProvider";
import DeviceSuggester from "./DeviceSuggester";
import AttributeSuggester from "./AttributeSuggester";

interface Props {
  tangoDB: string;
  device?: string;
  attribute?: string;
  label?: string;
  dataFormat?: "scalar" | "spectrum" | "image";
  dataType?: "numeric";
  onSelect?: (device: string | null, attribute: string | null, label: string | null) => void;
}

interface State {
  fetchingAttributes: boolean;
  attributes: Array<{
    name: string;
    label: string;
    datatype: string;
    dataformat: string;
  }>;
}

export default class AttributeSelect extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = { fetchingAttributes: false, attributes: [] };
    this.handleSelectDevice = this.handleSelectDevice.bind(this);
    this.handleSelectAttribute = this.handleSelectAttribute.bind(this);
  }

  public componentDidMount() {
    this.fetchAttributes();
  }

  public componentDidUpdate(prevProps) {
    if (this.props.device !== prevProps.device) {
      this.setState({ attributes: [] });
      this.fetchAttributes();
    }
  }

  public handleSelectDevice(newDevice: string) {
    this.fetchAttributes();
    const { onSelect } = this.props;
    if (onSelect && newDevice) {
      onSelect(newDevice, null, null);
    }
  }

  public handleSelectAttribute(newAttribute: string) {
    const attributes = this.filteredAttributes();
    const labels = attributes.map(({label}) => label);
    const names = attributes.map(({name}) => name);
    var indexLabel = 0;
    for(var i=0;i<labels.length;i++) if(labels[i]==newAttribute) indexLabel = i; 
    const { onSelect, device } = this.props;
    if (onSelect && device && newAttribute) {
      onSelect(device, names[indexLabel],labels[indexLabel]);
    }
  }

  public render() {
    const { device, attribute, label } = this.props;
    const attributes = this.filteredAttributes();
    const labels = attributes.map(({label}) => label);
    const names = attributes.map(({name}) => name);
    return (
      <DeviceConsumer>
        {({ devices }) => {
          const hasDevice = device != null && device !== "";
          return (
            <div className="AttributeSelect">
              <DeviceSuggester
                deviceName={device}
                devices={devices}
                onSelection={newValue => this.handleSelectDevice(newValue)}
              />
              <AttributeSuggester
                attributeName={attribute}
                attributeLabel = {label}
                labels = {labels}
                attributes={names}
                hasDevice={hasDevice}
                onSelection={newValue => this.handleSelectAttribute(newValue)}
              />
            </div>
          );
        }}
      </DeviceConsumer>
    );
  }

  private filteredAttributes() {
    const numericTypes = [
      "DevDouble",
      "DevFloat",
      "DevLong",
      "DevLong64",
      "DevShort",
      "DevUChar",
      "DevULong",
      "DevULong64",
      "DevUShort"
    ];

    return this.state.attributes.filter(attr => {
      const { dataType, dataFormat } = this.props;

      if (dataFormat === "scalar" && attr.dataformat !== "SCALAR") {
        return false;
      } else if (dataFormat === "spectrum" && attr.dataformat !== "SPECTRUM") {
        return false;
      } else if (dataFormat === "image" && attr.dataformat !== "IMAGE") {
        return false;
      } else if (
        dataType === "numeric" &&
        numericTypes.indexOf(attr.datatype) === -1
      ) {
        return false;
      } else {
        return true;
      }
    });
  }

  private async fetchAttributes() {
    const { device, tangoDB } = this.props;
    if (device) {
      this.setState({ attributes: [], fetchingAttributes: true });
      const attributes = await TangoAPI.fetchDeviceAttributes(tangoDB, device);
      this.setState({ attributes, fetchingAttributes: false });
    }
  }
}
