import React, { Component } from "react";
import { connect } from "react-redux";
import { IndexPath, Widget } from "../../types";
import { bundles } from "../../widgets";
import {
  DELETE_INPUT,
  ADD_INPUT,
  SET_INPUT,
  TOGGLE_INSPECTOR_COLLAPSED
} from "../../state/actionTypes";
import InputList from "./InputList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { RootState } from "../../state/reducers";
import { getInspectorCollapsed } from "../../state/selectors";

const MultipleSelection = () => (
  <div
    style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "100%"
    }}
  >
    <span
      style={{
        fontWeight: "bold",
        color: "lightgray"
      }}
    >
      Multiple selection of different type of widget
    </span>
  </div>
);
interface Props {
  tangoDB: string;
  widgets: Widget[];
  isRootCanvas: boolean;
  nbrSelectedWidgets: number;
  render: boolean;
  onSetInput: (path: IndexPath, value: any, widgetType?: string) => void;
  inspectorCollapsed: boolean;
  toggleInspectorCollapse: () => void;
  onDeleteInput: (path: IndexPath) => void;
  onAddInput: (path: IndexPath) => void;
}

class Inspector extends Component<Props> {
  public constructor(props: Props) {
    super(props);
    this.state = { collapsed: false };
  }
  public render() {
    if (!this.props.render) {
      return null;
    }

    const { widgets, tangoDB, inspectorCollapsed } = this.props;
    const definitions = bundles.map(bundle => bundle.definition);
    const definition = definitions.find(({ type }) => type === widgets[0].type);

    if (definition == null) {
      return null;
    }
    const isSameTypeOfWidget = widgets.reduce(
      (acc, curr) => acc && curr.type === widgets[0].type,
      true
    );
    return isSameTypeOfWidget ? (
      <div className="Inspector">
        <div className="InspectorWidgetName" onClick={this.props.toggleInspectorCollapse}>
          {definition.name}
          <FontAwesomeIcon
            title={`Hide the widget inspector \n (Alt+i)`}
            icon={"angle-left"}
            style={{
              float: "right",
              margin: "0em 0.5em 0.5em 1em",
              cursor: "pointer"
            }}
          />
        </div>
        {!inspectorCollapsed && (
          <InputList
            tangoDB={tangoDB}
            inputDefinitions={definition.inputs}
            inputs={widgets[0].inputs}
            onChange={(path, value) => this.props.onSetInput(path, value, widgets[0].type)}
            onDelete={path => this.props.onDeleteInput(path)}
            onAdd={path => this.props.onAddInput(path)}
            widgetType={widgets[0].type}
          />
        )}
      </div>
    ) : (
      <MultipleSelection />
    );
  }
}

function mapStateToProps(state: RootState) {
  return { inspectorCollapsed: getInspectorCollapsed(state) };
}
function mapDispatchToProps(dispatch) {
  return {
    onSetInput: (path: IndexPath, value: any, widgetType?: string) =>
      dispatch({ type: SET_INPUT, path, value, widgetType }),
    toggleInspectorCollapse: () =>
      dispatch({ type: TOGGLE_INSPECTOR_COLLAPSED }),
    onAddInput: (path: IndexPath) => dispatch({ type: ADD_INPUT, path }),
    onDeleteInput: (path: IndexPath) => dispatch({ type: DELETE_INPUT, path })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inspector);
