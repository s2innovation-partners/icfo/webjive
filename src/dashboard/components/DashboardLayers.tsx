import React, { Component } from "react";
import { Widget, Dashboard } from "../types";
import { connect } from "react-redux";
import { RootState } from "../state/reducers";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./DashboardLayers.css";
import { Dispatch } from "redux";
import {
  selectWidgets,
  reorderWidgets,
  deleteWidget
} from "../state/actionCreators";
import {
  getWidgets,
  getSelectedDashboard,
  getSelectedWidgets
} from "../state/selectors";
import { moveOrderIndices } from "../state/reducers/selectedDashboard/lib";

interface Props {
  selectedDashboard: Dashboard;
  widgets: Widget[];
  selectedWidgets: Widget[];
  onSelectWidgets: (ids: string[]) => void;
  onReorderWidget: (widgets: Widget[]) => void;
  onDeleteWidget: () => void;
  render: boolean;
}
class DashboardLayers extends Component<Props> {
  bodyRef: any;

  componentDidMount() {
    this.bodyRef.addEventListener("keyup", event => {
      const { selectedWidgets } = this.props;
      if (event.key === "ArrowUp") {
        this.moveWidgets(selectedWidgets, true);

        event.preventDefault();
      } else if (event.key === "ArrowDown") {
        this.moveWidgets(selectedWidgets, false);
        event.preventDefault();
      }
    });
  }

  public render() {
    const { widgets, selectedDashboard } = this.props;
    const displayStyle = this.props.render ? "block" : "none";
    return (
      <div
        style={{ display: displayStyle }}
        tabIndex={0}
        ref={ref => (this.bodyRef = ref)}
      >
        {widgets
          .sort((a, b) => a.order - b.order)
          .map(widget => (
            <this.WidgetLayer
              key={widget.id}
              id={widget.id}
              widget={widget}
              selected={selectedDashboard.selectedIds.includes(widget.id)}
            />
          ))}
        <Button
          title="Delete selected layers"
          onClick={this.props.onDeleteWidget}
          disabled={selectedDashboard.selectedIds.length === 0}
          className="btn btn-outline-secondary btn-sm btn-layer-action"
        >
          <FontAwesomeIcon icon="trash" />{" "}
        </Button>
        <Button
          title="Group selected layers"
          onClick={() => window.alert("not implemented yet")}
          disabled={selectedDashboard.selectedIds.length < 2}
          className="btn btn-outline-secondary btn-sm btn-layer-action"
        >
          <FontAwesomeIcon icon="layer-group" />{" "}
        </Button>
      </div>
    );
  }
  selectLayer = (e: any, id: string) => {
    const { selectedIds } = this.props.selectedDashboard;
    let ids: string[] = [];
    if (!e.shiftKey) {
      ids = [id];
    } else {
      if (selectedIds.includes(id)) {
        //deselect the layer
        ids = selectedIds.filter(x => x !== id);
      } else {
        //add the layer
        ids = selectedIds.concat([id]);
      }
    }
    this.props.onSelectWidgets(ids);
  };
  moveWidgets = (widgetsToMove: Widget[], up: boolean) => {
    const { widgets: allWidgets } = this.props;
    const newWidgets = moveOrderIndices(
      [ ...allWidgets ],
      [ ...widgetsToMove ],
      up
    );
    if (newWidgets){ //moveOrderIndices returns null if they cannot be moved, saved us from a state update and api call
      this.props.onReorderWidget(newWidgets);
    }
    
  };
  WidgetLayer = ({ id, widget, selected }) => {
    const label = widget.type.split("_").join(" ");
    let renderString = "";
    const inputs = widget.inputs;

    if (inputs.text !== undefined) {
      if (inputs.text) {
        renderString = inputs.text;
      } else {
        renderString = "No text";
      }
    } else if (inputs.attribute) {
      if (!inputs.attribute.device || !inputs.attribute.attribute) {
        renderString = "Undefined";
      } else {
        renderString =
          inputs.attribute.device + ":" + inputs.attribute.attribute;
      }
    } else if (inputs.command) {
      renderString = inputs.command.device + ":" + inputs.command.command;
    } else if (inputs.device) {
      renderString = inputs.device;
    } else if (inputs.attributes) {
      renderString = inputs.attributes.length + " attributes";
    } else {
    }
    if (renderString.length > 40) {
      renderString = renderString.slice(0, 37) + "...";
    }
    const selectedCss = selected ? "selected" : "";
    return (
      <div className={"widget-layer " + selectedCss}>
        <div style={{ float: "right" }}>
          <Button
            title={`Move this layer up\nHotkey: Arrow up`}
            className="btn arrow-button"
            onClick={() => this.moveWidgets([widget], true)}
          >
            <FontAwesomeIcon icon="arrow-alt-circle-up" />{" "}
          </Button>
          <Button
            title={`Move this layer up\nHotkey: Arrow down`}
            className="btn arrow-button"
            onClick={() => this.moveWidgets([widget], false)}
          >
            <FontAwesomeIcon icon="arrow-alt-circle-down" />{" "}
          </Button>
        </div>
        <div className="layer-content" onClick={e => this.selectLayer(e, id)}>
          <span className="label">
            {label} {widget.width}×{widget.height}
          </span>
          <div style={{ fontSize: "0.8em" }}>{renderString}</div>
        </div>
      </div>
    );
  };
}

function mapStateToProps(state: RootState) {
  return {
    selectedDashboard: getSelectedDashboard(state),
    widgets: getWidgets(state),
    selectedWidgets: getSelectedWidgets(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onSelectWidgets: (ids: string[]) => dispatch(selectWidgets(ids)),
    onReorderWidget: (widgets: Widget[]) => dispatch(reorderWidgets(widgets)),
    onDeleteWidget: () => dispatch(deleteWidget())
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardLayers);
