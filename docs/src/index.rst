WebJive
==============================================

Description
===========

Webjive is a web-based program that allows a user to create a visual interface using widgets, 
ùwhich may include charts, numerical indicators or dials that interface to Tango device back 
end database.

With this device explorer built on TangoGQL, you can:

1. View a list of all Tango devices
2. View and modify device properties
3. View and modify device attributes
4. View and execute device commands
5. Create web interfaces for interacting with Tango devices (on /&lt;tangoDB&gt;/dashboard)

Usage
=====

1. Clone the repository.

2. Run 

.. code-block:: bash

 npm install


3. Type 

.. code-block:: bash

 npm start


Minimum node version: 7.6 (introduced async/wait)

Verified working node version: 9.11.2 (currently used by the dockerfile)

Online demo
===========

https://webjive-demo.maxiv.lu.se/demodb (log in with demo/demo)

For developers
=====

.. toctree::
    label
    widget
    architecture
    display
    led
    command_writer
    boolean_display
    label_display

Authors
=====

WebJive was written by the KITS Group at MAX IV Laboratory .



.. toctree::
   :maxdepth: 2

   label
   widget
   architecture_overview
   architecture
   webjive_suite_candc
   webjive_suite_module_view
   boolean_display
   command_writer
   boolean_display
   led
   label_display

.. toctree::
   :caption: External resources:

    TangoGQL <https://web-maxiv-tangogql.readthedocs.io/en/latest/index.html>
    Webjive authorization <https://webjive-auth.readthedocs.io/en/latest/>
    Webjive Dashboard <https://webjive-dashboards.readthedocs.io/en/latest/>



Authors
=====

WebJive was written by the KITS Group at MAX IV Laboratory .




Prerequsities
-------------

To use this project, Docker >= v18 and GNU Make must be installed.

